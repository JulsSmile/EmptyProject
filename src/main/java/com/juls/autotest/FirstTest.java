package com.juls.autotest;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.*;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class FirstTest {

    private static WebDriver driver;

    @BeforeClass
    public static void setupClass() {
        WebDriverManager.chromedriver().setup();
    }

    @Before
    public void setup() {
       // System.setProperty("webdriver.chrome.driver", "C:/Program Files (x86)/chromedriver");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("https://mail.ukr.net/desktop/login");
    }

    @Test
    public void userLogin() {
        WebElement loginField = driver.findElement(By.id("id-1"));
        loginField.sendKeys("julshrenka");
        WebElement passwordField = driver.findElement(By.id("id-2"));
        passwordField.sendKeys("TestPassword42");
        WebElement loginButton = driver.findElement(By.cssSelector("body > div > div > main > form > button > div"));
        loginButton.click();
        WebElement profileUser = driver.findElement(By.cssSelector("#content > header > div.header__login > div.login > a > p"));
        String mailUser = profileUser.getText();
        Assert.assertEquals("julshrenka@ukr.net", mailUser);
    }

    @AfterClass
    public static void tearDown() {
        WebElement menuUser = driver.findElement(By.cssSelector(".login-button__menu-icon"));
        menuUser.click();
        WebElement logoutButton = driver.findElement(By.id("login__logout"));
        logoutButton.click();
        driver.quit();
    }
   }